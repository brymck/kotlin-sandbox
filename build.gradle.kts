import org.gradle.jvm.tasks.Jar
import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    val kotlinVersion = "1.2.51"
    application
    java
    kotlin("jvm") version kotlinVersion
    idea
    id("org.jetbrains.dokka") version "0.9.17"
    id("org.jlleitschuh.gradle.ktlint") version "4.1.0"
}

group = "com.brymck"
version = "0.1"

application {
    mainClassName = "com.brymck.sandbox.MainKt"
}

repositories {
    jcenter()
}

val ktlint by configurations.creating
dependencies {
    val junitVersion = "5.2.0"
    compile(kotlin("stdlib-jdk8"))
    ktlint("com.github.shyiko:ktlint:0.25.1")
    testCompile("org.junit.jupiter:junit-jupiter-api:$junitVersion")
    testCompile("org.junit.jupiter:junit-jupiter-params:$junitVersion")
    testRuntime("org.junit.jupiter:junit-jupiter-engine:$junitVersion")
}

tasks.withType<Test> {
    useJUnitPlatform()
    testLogging {
        events("passed", "skipped", "failed")
    }
}

tasks.withType<Wrapper> {
    gradleVersion = "4.9"
    distributionType = Wrapper.DistributionType.BIN
}

tasks.withType<KotlinCompile> {
    kotlinOptions.jvmTarget = "1.8"
}

val verifyKtlint by tasks.creating(JavaExec::class) {
    description = "Check Kotlin code style"
    classpath = ktlint
    main = "com.github.shyiko.ktlint.Main"
    args("src/**/*.kt")
}
tasks["check"].dependsOn(verifyKtlint)

val fatJar by tasks.creating(Jar::class) {
    baseName = "${project.name}-fat"
    manifest {
        attributes(mapOf(
            "Implementation-Title" to "Bryan's Kotlin sandbox",
            "Implementation-Version" to version,
            "Main-Class" to application.mainClassName
        ))
    }
    from(configurations.runtime.map { if (it.isDirectory) it else zipTree(it) })
    with(tasks["jar"] as CopySpec)
}
tasks["build"].dependsOn(fatJar)
