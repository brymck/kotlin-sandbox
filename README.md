Sandbox
=======

Just some Kotlin experiments

Installation
------------

```sh
git clone git@gitlab.com:brymck/kotlin-sandbox.git
cd kotlin-sandbox
./gradlew build run
```
