.PHONY: all clean lint test build docs run docker-build docker-run

all: lint build

clean:
	./gradlew clean

lint:
	./gradlew check

test:
	./gradlew test

build:
	./gradlew build

docs:
	./gradlew dokka

run: build
	./gradlew run

docker-build:
	sudo docker build --tag brymck/kotlin-sandbox .

docker-run:
	docker run --interactive --tty --rm brymck/kotlin-sandbox
