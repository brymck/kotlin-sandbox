FROM openjdk:alpine
LABEL maintainer="bryan.mckelvey@gmail.com"

WORKDIR /usr/src/kotlin-sandbox/
COPY . .

RUN ./gradlew build && \
    mv build/libs/sandbox-fat-*.jar sandbox-fat.jar && \
    ./gradlew clean && \
    rm -rf ~/.gradle/ ~/.kotlin/

ENTRYPOINT ["java", "-jar", "sandbox-fat.jar"]
