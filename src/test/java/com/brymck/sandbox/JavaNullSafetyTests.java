package com.brymck.sandbox;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class JavaNullSafetyTests {
    @Test
    public void lackOfNullSafetyAllowsNullPointerExceptionsAtRuntime() {
        JavaNullSafety jns = new JavaNullSafety();
        Assertions.assertThrows(NullPointerException.class, () -> {
            jns.calculateStringLength(null);
        });
        Assertions.assertEquals(5, jns.calculateStringLength("hello"));
    }
}
