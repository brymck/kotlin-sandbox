package com.brymck.sandbox

import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test

class NullSafetyTests {
    @Test
    fun `null safety prevents null pointer exceptions at runtime`() {
        // Uncomment the line below to demonstrate that it prevents compilation on a type mismatch
        // NullSafety().calculateStringLength(null)
        Assertions.assertEquals(5, NullSafety().calculateStringLength("hello"))
    }

    @Test
    fun `type checks allow smart type recognition`() {
        val nst = NullSafety()
        Assertions.assertEquals(5, nst.calculateStringLengthAllowingNulls("hello"))
        Assertions.assertNull(nst.calculateStringLengthAllowingNulls(null))
    }

    @Test
    fun `the Elvis operator is an alternative to conditional type checks`() {
        val nst = NullSafety()
        Assertions.assertEquals(5, nst.calculateStringLengthAllowingNullsSuccinct("hello"))
        Assertions.assertNull(nst.calculateStringLengthAllowingNullsSuccinct(null))
    }
}
