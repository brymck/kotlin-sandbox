package com.brymck.sandbox

object Helpers {
    fun getUserName(): String {
        return System.getenv("USER") ?: "you"
    }
}
