package com.brymck.sandbox

import com.brymck.sandbox.dataclasses.Money
import com.brymck.sandbox.dataclasses.plus
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test

class DataClassesTests {
    @Test
    fun `data class destructuring works`() {
        val tenBucks = Money(currency = "USD", amount = 10)
        val (currency, amount) = tenBucks
        Assertions.assertEquals("USD", currency)
        Assertions.assertEquals(10, amount)
    }

    @Test
    fun `addition operator overloading works`() {
        val tenBucks = Money(currency = "USD", amount = 10)
        val twentyBucks = Money(currency = "USD", amount = 20)
        val thirtyBucks = tenBucks + twentyBucks
        Assertions.assertEquals(30, thirtyBucks.amount)
    }

    @Test
    fun `adding different currencies fails`() {
        val tenBucks = Money(currency = "USD", amount = 10)
        val tenYen = Money(currency = "JPY", amount = 10)
        Assertions.assertThrows(IllegalArgumentException::class.java) {
            tenBucks + tenYen
        }
    }
}
