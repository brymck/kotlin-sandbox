package com.brymck.sandbox

import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test

class ExtensionsTests {
    @Test
    fun `String extension works`() {
        Assertions.assertEquals('o', "hello".last())
    }
}
