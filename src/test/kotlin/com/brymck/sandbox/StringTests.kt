package com.brymck.sandbox

import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test

class StringTests {
    @Test
    fun `string interpolation works`() {
        val name = "Bryan"
        Assertions.assertEquals("Hello, Bryan!", "Hello, $name!")
    }
}
