package com.brymck.sandbox.greeter

import com.brymck.sandbox.Helpers
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

class NormalGreeterTests {
    @Test
    fun `NormalGreeter greets you normally`() {
        val userName = Helpers.getUserName()
        assertEquals("Hello, $userName!", NormalGreeter().greet(userName))
    }
}
