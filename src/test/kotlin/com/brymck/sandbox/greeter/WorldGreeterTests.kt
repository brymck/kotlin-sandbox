package com.brymck.sandbox.greeter

import com.brymck.sandbox.Helpers
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

class WorldGreeterTests {
    @Test
    fun `Greeter says hello to the whole world`() {
        assertEquals("Hello, world!", WorldGreeter().greet(Helpers.getUserName()))
    }
}
