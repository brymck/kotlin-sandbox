package com.brymck.sandbox.greeter

import com.brymck.sandbox.Helpers
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

class VeryLoudGreeterTests {
    @Test
    fun `VeryLoudGreeter yells at you`() {
        val userName = Helpers.getUserName()
        val loudUserName = userName.toUpperCase()
        assertEquals("HELLO, $loudUserName!!!", VeryLoudGreeter().greet(userName))
    }
}
