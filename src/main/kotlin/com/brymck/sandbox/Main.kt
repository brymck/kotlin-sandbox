package com.brymck.sandbox

import com.brymck.sandbox.greeter.NormalGreeter

/**
 * The entry point.
 * @param args an array of [String]s containing command line arguments.
 */
fun main(args: Array<String>) {
    val name = args.getOrElse(0) { "world" }
    println(NormalGreeter().greet(name))
}
