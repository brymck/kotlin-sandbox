package com.brymck.sandbox.interfaces

/**
 * An interface for something that greets someone (possibly) by name.
 */
interface Greeter {
    /**
     * Greets someone (probably) by their [name].
     * @param name the person's name.
     * @return the greeting.
     */
    fun greet(name: String): String
}
