package com.brymck.sandbox.dataclasses

data class Money(val currency: String, val amount: Int)

operator fun Money.plus(other: Money): Money {
    if (currency != other.currency) {
        throw IllegalArgumentException("Currency code ${other.currency} does not match $currency")
    }
    return copy(amount = amount + other.amount)
}
