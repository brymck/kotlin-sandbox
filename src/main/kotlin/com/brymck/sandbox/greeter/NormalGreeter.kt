package com.brymck.sandbox.greeter

import com.brymck.sandbox.interfaces.Greeter

/**
 * A much more normal greeter.
 */
class NormalGreeter : Greeter {
    override fun greet(name: String): String {
        return "Hello, $name!"
    }
}
