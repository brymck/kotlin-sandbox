package com.brymck.sandbox.greeter

import com.brymck.sandbox.interfaces.Greeter

/**
 * A VERY LOUD GREETER.
 */
class VeryLoudGreeter : Greeter {
    override fun greet(name: String): String {
        val loudName = name.toUpperCase()
        return "HELLO, $loudName!!!"
    }
}
