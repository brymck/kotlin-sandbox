package com.brymck.sandbox.greeter

import com.brymck.sandbox.interfaces.Greeter

/**
 * A [Greeter] that focuses on the entire world.
 */
class WorldGreeter : Greeter {
    /**
     * Greets the entire world and just totally ignores [name].
     * @param name the person's name.
     * @return the greeting.
     */
    @Suppress("UNUSED_PARAMETER")
    override fun greet(name: String): String {
        return "Hello, world!"
    }
}
