package com.brymck.sandbox

/**
 * A Kotlin null safety example.
 *
 * See [JavaNullSafety] for a Java example that will compile but fail at runtime when its one method is passed null.
 */
class NullSafety {
    /**
     * Calculate the length of a string.
     * @param str string.
     * @return length of the string.
     */
    fun calculateStringLength(str: String): Int = str.length

    /**
     * Calculate the length of a string, allowing nulls. This also illustrates smart type casting.
     * @param str string.
     * @return length of the string where available, otherwise null.
     */
    fun calculateStringLengthAllowingNulls(str: String?): Int? {
        if (str == null) {
            return null
        }
        return str.length
    }

    /**
     * Calculate the length of a string, allowing nulls.
     * @param str string.
     * @return length of the string where available, otherwise null.
     */
    fun calculateStringLengthAllowingNullsSuccinct(str: String?): Int? = str?.length
}
