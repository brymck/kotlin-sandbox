package com.brymck.sandbox

fun String.last(): Char = this[length - 1]
