package com.brymck.sandbox;

/**
 * A Java (lack of) null safety example.
 */
public class JavaNullSafety {
    /**
     * Calculate the length of a string.
     * @param str string.
     * @return length of the string.
     */
    public int calculateStringLength(String str) {
        return str.length();
    }
}
